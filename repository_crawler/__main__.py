import logging
import threading
import time

from sqlalchemy import create_engine
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.schema import DropTable

import repository_crawler.config as config
from repository_crawler.crawler.bitbucket import BitbucketDetailsCrawler
from repository_crawler.crawler.bitbucket import BitbucketCrawler
from repository_crawler.crawler.github import GithubTopicsCrawler
from repository_crawler.crawler.github import GithubCrawler
from repository_crawler.crawler.github import GithubDetailsCrawler
from repository_crawler.crawler.gitlab import GitlabCrawler
from repository_crawler.crawler.gitlab import GitlabDetailsCrawler
from repository_crawler.model.base import Base
from repository_crawler.model.platform_manager import PlatformManager


@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    return compiler.visit_drop_table(element) + " CASCADE"


def main():
    logging.basicConfig(level=config.LOGLEVEL, format='%(asctime)s [%(levelname)s] (%(threadName)-10s) %(message)s')

    # Create database connection
    engine = create_engine(
        config.DB_URL,
        connect_args={'options': f"-csearch_path={config.DB_SCHEMA}"},
        echo=(config.LOGLEVEL == "DEBUG")
    )
    # Drop all tables in DEV mode
    if config.MODE == "DEV":
        Base.metadata.drop_all(engine)
    # Create table structure if not existent
    # Note: all model classes need to be imported in the main __init__.py
    Base.metadata.create_all(engine)

    # Add platforms
    platform_manager = PlatformManager()
    platform_manager.add_platform_if_not_exist(platform_id=config.GITHUB_ID, name="GitHub")
    platform_manager.add_platform_if_not_exist(platform_id=config.GITLAB_ID, name="GitLab")
    platform_manager.add_platform_if_not_exist(platform_id=config.BITBUCKET_ID, name="Bitbucket")

    # Crawl topics first to get a keyword base for topic extraction
    GithubTopicsCrawler().crawl()

    run_event = threading.Event()
    run_event.set()

    threads = []

    # Crawl repositories
    if config.CRAWL_GITHUB:
        github_crawler = threading.Thread(name="GithubCrawler", target=GithubCrawler(run_event).crawl)
        github_crawler.start()
        threads.append(github_crawler)
        github_details_crawler = threading.Thread(name="GithubDetailsCrawler",
                                                  target=GithubDetailsCrawler(run_event).crawl)
        github_details_crawler.start()
        threads.append(github_details_crawler)

    if config.CRAWL_GITLAB:
        gitlab_crawler = threading.Thread(name="GitlabCrawler", target=GitlabCrawler(run_event).crawl)
        gitlab_crawler.start()
        threads.append(gitlab_crawler)
        gitlab_details_crawler = threading.Thread(
            name="GitlabDetailsCrawler", target=GitlabDetailsCrawler(run_event).crawl)
        gitlab_details_crawler.start()
        threads.append(gitlab_details_crawler)

    if config.CRAWL_BITBUCKET:
        bitbucket_crawler = threading.Thread(name="BitbucketCrawler", target=BitbucketCrawler(run_event).crawl)
        bitbucket_crawler.start()
        threads.append(bitbucket_crawler)
        bitbucket_details_crawler = threading.Thread(name="BitbucketDetailsCrawler",
                                                     target=BitbucketDetailsCrawler(run_event).crawl)
        bitbucket_details_crawler.start()
        threads.append(bitbucket_details_crawler)

    try:
        while True:
            time.sleep(.1)
    except KeyboardInterrupt:
        logging.info("KeyboardInterrupt received. Attempting to close threads...")
        run_event.clear()
        for thread in threads:
            logging.info(f"Waiting for thread {thread.name} to close...")
            thread.join()
        logging.info("All threads closed successfully")


if __name__ == '__main__':
    main()
