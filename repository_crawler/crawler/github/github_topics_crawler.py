import json
import logging

import repository_crawler.config as config
from repository_crawler.crawler.crawler_utils import check_rate_limit
from repository_crawler.crawler.crawler_utils import get_next_url
from repository_crawler.crawler.crawler_utils import get_request
from repository_crawler.model.topic import Topic
from repository_crawler.model.topic_manager import TopicManager


class GithubTopicsCrawler:
    def __init__(self, per_page=100):
        self.per_page = per_page
        self.topic_manager = TopicManager()

    def crawl(self):
        if self.topic_manager.topics_exist():
            logging.info(f"Topics were already crawled - skipping GithubTopicsCrawler")
            return

        self._crawl_topics("is:featured")
        self._crawl_topics("is:curated")

    def _crawl_topics(self, query, page=1, per_page=100):
        logging.info(f"Crawling topics with query {query}")

        url = config.GITHUB_API_SEARCH_TOPICS
        parameters = {"q": query, "page": page, "per_page": per_page}

        while True:
            # Make request
            response = get_request(url, parameters, token=config.GITHUB_API_KEY,
                                   accept="application/vnd.github.mercy-preview+json")

            # Parse topics
            for topic in json.loads(response.content)["items"]:
                self._parse_topic(topic)

            url = get_next_url(response)
            parameters = {}
            if url is None:
                break

            # Check rate limit
            check_rate_limit(response, remaining_key="X-RateLimit-Remaining", reset_key="X-RateLimit-Reset")

    def _parse_topic(self, topic_dict):
        logging.debug(f"Parsing topic {topic_dict}")

        topic = Topic(
            name=topic_dict["name"],
            featured=topic_dict["featured"],
            curated=topic_dict["curated"]
        )

        self.topic_manager.add_topic_if_not_exist(topic)
