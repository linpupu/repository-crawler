import json
import logging
import time

from dateutil import parser

import repository_crawler.config as config
from repository_crawler.model.repository_manager import RepositoryManager
from repository_crawler.model.setting_manager import SettingManager
from repository_crawler.model.user_manager import UserManager
from repository_crawler import User
from repository_crawler.crawler.crawler_utils import get_request

REST_API = "https://api.bitbucket.org/2.0/"

REST_API_PROJECTS = REST_API + "repositories"

# nickname + project name + branch/commit + path to file
REST_API_PROJECT_DETAILS = REST_API_PROJECTS + "/{%s}/%s"
LAST_PAGE_CRAWLED_KEY = "bitbucket.last_page_crawled"


class BitbucketCrawler:
    def __init__(self, run_event, per_page=100):
        self.repository_manager = RepositoryManager()
        self.user_manager = UserManager()
        self.setting_manager = SettingManager()
        self.after = self.setting_manager.get_setting(LAST_PAGE_CRAWLED_KEY, "")
        self.run_event = run_event
        self.per_page = per_page

    def crawl(self):
        url = REST_API_PROJECTS
        parameters = {"fields": "-values.links,"
                                "-values.website,"
                                "-values.for_policy,"
                                "-values.mainbranch,"
                                "-values.owner.links,"
                                "-values.type,"
                                "-values.is_private",
                      "pagelen": self.per_page}
        if self.after != "":
            parameters["after"] = self.after

        while self.run_event.is_set():
            # Make request
            response = get_request(url, parameters, token=config.BITBUCKET_API_KEY, basic=True)

            # Parse projects
            if response is None:
                time.sleep(config.DETAILS_CRAWLER_SLEEP_TIME)
                continue

            content = json.loads(response.content)
            for project in content["values"]:
                self._parse_project(project)

            # Update pages done
            last_project = content["values"][-1]["created_on"]
            logging.info(f"Successfully parsed  repositories until {last_project}")
            self.setting_manager.update_setting(LAST_PAGE_CRAWLED_KEY, last_project)

            # Determine next url
            url = content["next"]
            parameters = {}
            if url is None:
                break

    def _parse_project(self, project):
        logging.debug(f"Bitbucket: Parsing project {project}")

        owner = User(
            external_id=project['owner']['uuid'][1:-1],
            platform_id=config.BITBUCKET_ID
        )

        if 'nickname' in project['owner']:
            owner.name = project['owner']['nickname']
        elif 'username' in project['owner']:
            owner.name = project['owner']['username']

        # No date
        if not project["updated_on"]:
            return

        # Project is too old
        if parser.parse(project["updated_on"]) < config.LAST_UPDATED_THRESHOLD:
            return

        repository = self.repository_manager.get_or_create_repository(external_id=project["uuid"][1:-1],
                                                                      platform_id=config.BITBUCKET_ID)
        repository.description = project["description"]
        repository.name = project["name"]
        repository.created = project['created_on']
        repository.last_updated = project["updated_on"]

        # get source branch
        if "mainbranch" in project:
            mainbranch = project["mainbranch"]["name"]
        else:
            if project["scm"] == "hg":
                # Mercurial
                mainbranch = "default"
            else:
                # e.g. project["scm"] = "git"
                # assume mainbranch is master
                mainbranch = "master"

        repository.readme_url = REST_API_PROJECT_DETAILS % (owner.external_id, repository.name) + "/src/" + mainbranch

        user = self.user_manager.add_user_if_not_exist(owner)
        repository.owner_id = user.id
        self.repository_manager.add_repository(repository)
        self.repository_manager.add_owner(repository, user)
        # project["language"] is main language if exists
        if project["language"] != "":
            self.repository_manager.add_language_to_repo(repository, project["language"])
        self.repository_manager.commit()
