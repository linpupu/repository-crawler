import json
import logging
import time

import repository_crawler.config as config
from repository_crawler.crawler.details_crawler import DetailsCrawler
from repository_crawler import User
from repository_crawler.crawler.crawler_utils import get_request
from repository_crawler.model.repository import Repository
from repository_crawler.model.user_manager import UserManager

REST_API = "https://api.bitbucket.org/2.0/"

REST_API_PROJECTS = REST_API + "repositories"

# nickname + project name + branch/commit + path to file
REST_API_PROJECT_DETAILS = REST_API_PROJECTS + "/{%s}/%s"


class BitbucketDetailsCrawler(DetailsCrawler):
    def __init__(self, run_event):
        super().__init__()
        self.user_manager = UserManager()
        self.run_event = run_event

    def crawl(self):
        while self.run_event.is_set():
            repositories = self.repository_manager.get_repositories_without_details(config.BITBUCKET_ID)

            if len(repositories) == 0:
                logging.info(f"Sleeping for {config.DETAILS_CRAWLER_SLEEP_TIME} seconds")
                time.sleep(config.DETAILS_CRAWLER_SLEEP_TIME)
            else:
                for repository in repositories:
                    logging.info(f"Crawling details for repository with ID {repository.id}")
                    if self._crawl_watchers(repository) and self._crawl_readme_and_topics(repository):
                        repository.details_crawled = True
                    else:
                        self.repository_manager.delete(repository)

                    self.repository_manager.commit()

                    if not self.run_event.is_set():
                        break

    def _crawl_readme_and_topics(self, repository: Repository):
        logging.debug(f"Crawling README for repository with ID {repository.id}")

        readme = self._get_readme(repository)

        if readme is None:
            if repository.description is None:
                return None
            else:
                merged_description = repository.description
        else:
            if isinstance(readme, bytes):
                readme = readme.decode('utf-8')
            merged_description = repository.description + " " + readme

        return self._process_readme(merged_description, repository)

    def _crawl_watchers(self, repo: Repository):
        url = REST_API_PROJECT_DETAILS % (repo.owner.external_id, repo.name)

        # Get visitors / watchers
        url_watchers = url + "/watchers"
        response = get_request(url_watchers, token=config.BITBUCKET_API_KEY, basic=True)
        if response is not None:
            content = json.loads(response.content)

            if "values" not in content:
                logging.info(f"Value does not exist for repository with ID {repo.id}")
                return False

            # Ignore all repositories below threshold
            if len(content["values"]) < config.WATCHERS_THRESHOLD:
                return False

            for watcher in content["values"]:
                if watcher['type'] == 'user':
                    user = User(
                        external_id=watcher['uuid'][1:-1],
                        platform_id=config.BITBUCKET_ID,
                        name=watcher['nickname']
                    )
                    user = self.user_manager.add_user_if_not_exist(user)
                    self.repository_manager.add_watcher(repo, user)
            return True
        return False

    def _get_readme(self, repo: Repository):
        possible_urls = ["/README.md",
                         "/README.txt",
                         "/README.rst",
                         "/README.adoc",
                         "/README.rdoc",
                         "/docs/README.md",
                         "/docs/README.txt",
                         "/docs/README.rst",
                         "/docs/README.adoc",
                         "/docs/README.rdoc"]

        for url in possible_urls:
            response = get_request(repo.readme_url + url, token=config.BITBUCKET_API_KEY, basic=True)
            if response is not None:
                # save readme url
                repo.readme_url = url
                try:
                    decoded_content = response.content.decode('utf-8')
                except UnicodeError:
                    return None
                return decoded_content

        return None
