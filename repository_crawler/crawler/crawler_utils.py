import logging
import time

import requests
from requests.utils import parse_header_links

import repository_crawler.config as config


def get_request(url, parameters=None, retries=5, token=None, basic=False, accept=None):
    if parameters is None:
        parameters = {}
    if token is None:
        headers = {}
    elif not basic:
        headers = {"Authorization": f"Bearer {token}"}
    else:
        headers = {"Authorization": f"Basic {token}"}
    if accept is not None:
        headers['Accept'] = accept

    logging.info(f"Sending GET to {url}")

    while retries >= 0:
        response = requests.get(
            url,
            headers=headers,
            params=parameters
        )

        if response.status_code == 200:
            return response
        elif response.status_code in [403, 404, 410, 451]:
            return None
        elif response.status_code == 429:
            logging.info(
                f"URL: {url}\n"
                f"Parameters: {parameters}\n"
                f"Rate limit reached - sleeping for {config.RATE_LIMIT_SLEEP_TIME} seconds"
            )
            time.sleep(config.RATE_LIMIT_SLEEP_TIME)
            retries = 5
        elif response.status_code == 500:
            logging.info(
                f"URL: {url}\n"
                f"Parameters: {parameters}\n"
                f"Internal server error - sleeping for {config.INTERNAL_SERVER_ERROR_SLEEP_TIME} seconds"
            )
            time.sleep(config.INTERNAL_SERVER_ERROR_SLEEP_TIME)
            retries -= 1
        else:
            if retries == 0:
                raise Exception(
                    f"URL:{url}\n"
                    f"Parameters: {parameters}\n"
                    f"Error code: {response.status_code} \nResponse: {response.text}")
            else:
                logging.info(f"Retrying request in {config.REQUEST_ERROR_SLEEP_TIME} seconds")
                time.sleep(config.REQUEST_ERROR_SLEEP_TIME)
                retries -= 1

    return None


def get_next_url(response):
    if "Link" in response.headers:
        for link in parse_header_links(response.headers["Link"]):
            if "rel" in link:
                if link["rel"] == "next":
                    return link["url"]
    return None


def check_rate_limit(response, remaining_key, reset_key, threshold=1):
    if response is None:
        return

    if int(response.headers[remaining_key]) <= threshold:
        sleep_time = int(response.headers[reset_key]) - int(time.time())
        if sleep_time > 0:
            logging.info(f"Rate limit reached - sleeping for {sleep_time} seconds")
            time.sleep(sleep_time)
