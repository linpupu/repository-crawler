import base64
import json
import logging
import time

import repository_crawler.config as config
from repository_crawler.crawler.details_crawler import DetailsCrawler
from repository_crawler import User
from repository_crawler.crawler.crawler_utils import check_rate_limit
from repository_crawler.crawler.crawler_utils import get_next_url
from repository_crawler.crawler.crawler_utils import get_request
from repository_crawler.model.repository import Repository
from repository_crawler.model.user_manager import UserManager


class GitlabDetailsCrawler(DetailsCrawler):
    def __init__(self, run_event):
        super().__init__()
        self.user_manager = UserManager()
        self.run_event = run_event
        self.per_page = 100

    def crawl(self):
        while self.run_event.is_set():
            repositories = self.repository_manager.get_repositories_without_details(config.GITLAB_ID)

            if len(repositories) == 0:
                logging.info(f"Sleeping for {config.DETAILS_CRAWLER_SLEEP_TIME} seconds")
                time.sleep(config.DETAILS_CRAWLER_SLEEP_TIME)
            else:
                for repository in repositories:
                    logging.info(f"Crawling details for repository with ID {repository.id}")
                    if self._crawl_readme(repository):
                        self._crawl_language(repository)
                        self._crawl_starrers(repository)
                        repository.details_crawled = True
                    else:
                        self.repository_manager.delete(repository)

                    self.repository_manager.commit()

                    if not self.run_event.is_set():
                        break

    def _crawl_language(self, repository: Repository):
        logging.debug(f"Crawling language for repository with ID {repository.id}")

        response = get_request(
            config.GITLAB_API_PROJECT_LANGUAGES % repository.external_id,
            token=config.GITLAB_API_KEY
        )
        languages = json.loads(response.content)

        if len(languages) > 0:
            # Only save main language
            self.repository_manager.add_language_to_repo(repository, list(languages)[0])

        check_rate_limit(response, remaining_key="RateLimit-Remaining", reset_key="RateLimit-Reset")

    def _crawl_starrers(self, repository: Repository):
        logging.debug(f"Crawling starrers for repository with ID {repository.id}")

        url = config.GITLAB_API_PROJECT_STARRERS % repository.external_id
        parameters = {'per_page': self.per_page}

        while True:
            response = get_request(url, parameters, token=config.GITLAB_API_KEY)

            for starrer in json.loads(response.content):
                user = User(
                    external_id=str(starrer['user']['id']),
                    platform_id=config.GITLAB_ID,
                    name=starrer['user']['username']
                )

                user = self.user_manager.add_user_if_not_exist(user)
                self.repository_manager.add_starrer(repository, user)

            url = get_next_url(response)
            parameters = {}

            if url is None:
                break

            check_rate_limit(response, remaining_key="RateLimit-Remaining", reset_key="RateLimit-Reset")

    def _crawl_readme(self, repository: Repository):
        logging.debug(f"Crawling README for repository with ID {repository.id}")

        response = get_request(repository.readme_url, token=config.GITLAB_API_KEY)
        check_rate_limit(response, remaining_key="RateLimit-Remaining", reset_key="RateLimit-Reset")

        if response is None:
            return False

        file_info = json.loads(response.content)
        readme = str(base64.b64decode(file_info['content']).decode('utf-8'))

        return self._process_readme(readme, repository)
