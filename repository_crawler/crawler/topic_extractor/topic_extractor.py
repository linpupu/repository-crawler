import re

import docutils.core
import spacy
from bs4 import BeautifulSoup
from markdown import markdown
from spacy_langdetect import LanguageDetector

from repository_crawler.model.topic_manager import TopicManager

nlp = spacy.load('en')
nlp.add_pipe(LanguageDetector(), name='language_detector', last=True)
topic_manager = TopicManager()
topics = topic_manager.get_all()


def markdown_to_text(markdown_string):
    """ Converts a markdown string to plaintext """

    # md -> html -> text since BeautifulSoup can extract text cleanly
    return html_to_text(markdown(markdown_string))


def rst_to_text(text):
    # rst -> html -> text since BeautifulSoup can extract text cleanly
    return html_to_text(docutils.core.publish_string(source=text).decode('utf-8'))


def adoc_to_text(text):
    # remove code snippts
    text = re.sub(r'----(.*?)----', ' ', text)
    text = re.sub(r'\s`(.*?)`\s', ' ', text)
    text = re.sub(r'\s``(.*?)``', ' ', text)
    text = re.sub(r'`+(.*?)+`', ' ', text)

    # remove images
    text = re.sub(r'image::(.*)\[', ' ', text)
    text = re.sub(r'image:(.*)\[', ' ', text)

    # remove links
    text = re.sub(r'link:(.*)\[', ' ', text)
    text = re.sub(r'xref:(.*)\[', ' ', text)
    text = re.sub(r'http://(.*)\[', ' ', text)
    text = re.sub(r'https://(.*)\[', ' ', text)

    return text


def rdoc_to_text(text):
    # remove code snippts
    text = re.sub(r'^ {2}(.*?)\n', ' ', text, flags=re.MULTILINE | re.DOTALL)
    text = re.sub(r'<tt>(.*?)</tt>', ' ', text)
    text = re.sub(r'\+(.*?)\+', ' ', text)

    # remove links
    text = re.sub(r'\[http://(.*)\]', ' ', text)
    text = re.sub(r'\[https://(.*)\]', ' ', text)

    return text


def html_to_text(html):
    # remove code snippets
    html = re.sub(r'<pre>(.*?)</pre>', ' ', html)
    html = re.sub(r'<code>(.*?)</code >', ' ', html)

    # extract text
    soup = BeautifulSoup(html, "html.parser")
    return ''.join(soup.findAll(text=True))


def get_chunks(text):
    # Split text if necessary since only 1000000 characters can be processed with spaCy by default
    return [text.lower()[i:i + 1000000] for i in range(0, len(text), 1000000)]


def check_language(text):
    for chunk in get_chunks(text):
        doc = nlp(chunk.lower(), disable=['ner'])
        if doc._.language['language'] != "en":
            return False
    return True


def data_preprocessing(text):
    valid_tokens = []

    for chunk in get_chunks(text):
        # 1. Tokenization
        doc = nlp(chunk.lower(), disable=['ner'])

        # Check language
        if doc._.language['language'] != "en":
            return []

        # 2. Remove stop words and other words that don't match the topic regex
        for token in doc:
            if token.is_stop or token.is_punct:
                continue
            if re.fullmatch("[a-zA-Z0-9-]*", token.orth_) is None:
                continue
            # 3. Get each word's base form
            valid_tokens.append(token.lemma_)
    return valid_tokens


def get_topics_for_text(text):
    clean_tokens = data_preprocessing(text)
    matched_topics = []

    for topic in topics:
        # Split topic if necessary
        sub_topics = topic.name.split("-")
        sub_topic_count = []

        for sub_topic in sub_topics:
            # Lemmatize topic
            doc = nlp(sub_topic, disable=['ner', 'parser'])
            # Count occurrences of topic in text
            num = clean_tokens.count(doc[0].lemma_)
            sub_topic_count.append(num)

        # each sub topic must be present
        if sub_topic_count.count(0) > 0:
            continue

        # calculate average number of occurences
        num = sum(sub_topic_count) / len(sub_topic_count)
        matched_topics.append((topic, num))

    # return matched topics
    return matched_topics
