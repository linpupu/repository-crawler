from sqlalchemy import Column, String

from .base import Base


class Setting(Base):
    __tablename__ = 'setting'

    # Columns
    key = Column(String, primary_key=True)
    value = Column(String, nullable=False)
