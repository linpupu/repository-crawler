import logging

from repository_crawler import Setting
from .session_manager import SessionManager


class SettingManager(SessionManager):

    def get_setting(self, key, default=None):
        setting = self.session.query(Setting).filter_by(key=key).first()
        if setting:
            return setting.value
        else:
            return default

    def update_setting(self, key, value):
        if value is None:
            logging.warning(f"Cannot update setting {key} with None")
            return

        existing_setting = self.session.query(Setting).filter_by(key=key).first()

        if existing_setting:
            existing_setting.value = value
        else:
            setting = Setting(key=key, value=value)
            self.session.add(setting)

        self.session.commit()
