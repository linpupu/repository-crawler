from sqlalchemy import Column, Integer, ForeignKey, Float
from sqlalchemy.orm import relationship

from .base import Base


class RepositoryTopic(Base):
    __tablename__ = 'repository_topic'

    # Columns
    repository_id = Column(Integer, ForeignKey('repository.id'), primary_key=True)
    topic_id = Column(Integer, ForeignKey('topic.id'), primary_key=True)
    weight = Column(Float)

    # Relationships
    repository = relationship('Repository', back_populates='topics')
    topic = relationship('Topic', back_populates='repositories')
