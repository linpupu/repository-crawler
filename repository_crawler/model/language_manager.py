from repository_crawler import Language
from .session_manager import SessionManager


class LanguageManager(SessionManager):

    def get_language_by_name_and_create(self, name):
        language = self.session.query(Language).filter_by(name=name).first()
        if language:
            return language
        else:
            language = Language(name=name)
            self.session.add(language)
            self.session.commit()
            return language
