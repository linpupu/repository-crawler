from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

import repository_crawler.config as config


class SessionManager(object):
    def __init__(self):
        engine = create_engine(config.DB_URL, echo=(config.LOGLEVEL == "DEBUG"))
        self.session = scoped_session(sessionmaker(bind=engine))()

    def __del__(self):
        self.session.close()
