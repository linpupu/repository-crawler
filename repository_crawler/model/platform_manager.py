from repository_crawler import Platform
from .session_manager import SessionManager


class PlatformManager(SessionManager):

    def add_platform_if_not_exist(self, platform_id, name):
        if self.session.query(Platform).filter_by(id=platform_id).count() == 0:
            platform = Platform(id=platform_id, name=name)
            self.session.add(platform)
            self.session.commit()
