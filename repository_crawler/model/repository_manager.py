import repository_crawler.config as config
from repository_crawler import User, RepositoryUserVisit, Topic, RepositoryTopic
from repository_crawler.model.language_manager import LanguageManager
from .repository import Repository
from .session_manager import SessionManager


class RepositoryManager(SessionManager):
    def __init__(self):
        self.language_manager = LanguageManager()
        super().__init__()

    def repository_exist(self, external_id, platform_id):
        return self.session.query(Repository).filter_by(external_id=external_id,
                                                        platform_id=platform_id).first() is not None

    def get_or_create_repository(self, external_id, platform_id):
        old_repo = self.session.query(Repository).filter_by(external_id=external_id,
                                                            platform_id=platform_id).first()
        if old_repo:
            return old_repo
        else:
            return Repository(external_id=external_id, platform_id=platform_id)

    def get_repositories_without_details(self, platform_id):
        return self.session.query(Repository).filter_by(platform_id=platform_id,
                                                        details_crawled=False).limit(10).all()

    def add_language_to_repo(self, repository: Repository, language):
        language = self.language_manager.get_language_by_name_and_create(language.lower())
        repository.language_id = language.id
        self.session.commit()

    def add_owner(self, repository: Repository, user: User):
        self.add_user_association(repository, user, config.OWNER_WEIGHT)

    def add_watcher(self, repository: Repository, user: User):
        self.add_user_association(repository, user, config.WATCHER_WEIGHT)

    def add_starrer(self, repository: Repository, user: User):
        self.add_user_association(repository, user, config.STARRER_WEIGHT)

    def add_user_association(self, repository: Repository, user: User, weight):
        old_assoc = self.session.query(RepositoryUserVisit).filter_by(repository_id=repository.id,
                                                                      user_id=user.id).first()
        if old_assoc:
            # Only keep strongest association. Could be relevant if user is e. g. owner and watcher at the same time
            old_assoc.weight = max(old_assoc.weight, weight)
        else:
            repo_visitor_assoc = RepositoryUserVisit(repository_id=repository.id, user_id=user.id, weight=weight)
            self.session.add(repo_visitor_assoc)

    def add_repository(self, repository: Repository):
        self.session.add(repository)

    def commit(self):
        self.session.commit()

    def delete(self, repo):
        self.session.delete(repo)
        self.session.commit()

    def add_topic_to_repo(self, repository: Repository, topic: Topic, weight):
        old_assoc = self.session.query(RepositoryTopic).filter_by(repository_id=repository.id,
                                                                  topic_id=topic.id).first()
        if old_assoc:
            old_assoc.weight = weight
        else:
            repo_topic_assoc = RepositoryTopic(repository_id=repository.id, topic_id=topic.id, weight=weight)
            repository.topics.append(repo_topic_assoc)
            self.session.add(repo_topic_assoc)
        self.session.commit()

    def add_topics_to_repo(self, repository: Repository, topics):
        total_weight = sum(n for _, n in topics)
        for topic in topics:
            self.add_topic_to_repo(repository, topic[0], topic[1] / total_weight)

    def add_weighted_topics_to_repo(self, repository: Repository, weighted_topics):
        for topic in weighted_topics:
            self.add_topic_to_repo(repository, topic[0], topic[1])
