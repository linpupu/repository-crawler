from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship

from .base import Base


class User(Base):
    __tablename__ = 'user'
    __table_args__ = (UniqueConstraint('external_id', 'platform_id', name='user_platform_uc'),)

    # Columns
    id = Column(Integer, primary_key=True)
    external_id = Column(String, nullable=False)
    platform_id = Column(Integer, ForeignKey('platform.id'), nullable=False)
    name = Column(String, nullable=False)

    # Relationships
    platform = relationship("Platform", foreign_keys=[platform_id])
    owned_repositories = relationship('Repository', back_populates='owner', foreign_keys='Repository.owner_id')
    visited_repositories = relationship('RepositoryUserVisit', back_populates='user')
