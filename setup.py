from setuptools import setup

setup(
    name='repository_crawler',
    version='0.1.0',
    packages=['repository_crawler'],
    entry_points={
        'console_scripts': [
            'repository_crawler = repository_crawler.__main__:main'
        ]
    })
