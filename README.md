# Repository Crawler

This repository contains an API crawler for GitHub, GitLab and Bitbucket that crawls details of repositories into an integrated data schema.

## Installation

As a prerequisite for setting up the crawler a PostgreSQL database is needed. In the [infrastructure repository](https://gitlab.com/data-integration/infrastructure) more details on how this could be done using Docker can be found. Furthermore, API keys for the repsective platforms GitHub, GitLab and Bitbucket are required. To start a Docker container running the repository crawler the provided `docker-compose.yml` file can be used:

```sh
$ docker-compose up -d
```

The connection details for the database, the API keys, as well as additional configuration options can be provided using environment variables:

* `BACKEND_NET`: The name of a Docker network with access to the PostgreSQL database.
* `POSTGRES_HOST`: The PostgreSQL host.
* `POSTGRES_DB`: The PostgreSQL database.
* `POSTGRES_USER`: The PostgreSQL user.
* `POSTGRES_PASSWORD`: The PostgreSQL password.
* `RC_BITBUCKET_API_KEY`: The Bitbucket API key.
* `RC_GITHUB_API_KEY`: The GitHub API key.
* `RC_GITLAB_API_KEY`: The GitLab API key.
